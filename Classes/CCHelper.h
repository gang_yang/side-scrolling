#pragma once

/*
ccpToAngle : 得到弧度
CC_RADIANS_TO_DEGREES: 把弧度转换为角度
*/
#include "cocos2d.h"
USING_NS_CC;

inline void debug_Rect(const char *name, CCRect rect)
{
    CCLog("%s -> [%lf,%lf] [%lf,%lf]", name, rect.origin.x, rect.origin.y
                               , rect.size.width, rect.size.height);
}
#define gDirector CCDirector::sharedDirector()

#define gTouchDispatcher CCDirector::sharedDirector()->getTouchDispatcher()

#define gSpriteFrameCache CCSpriteFrameCache::sharedSpriteFrameCache()

namespace CCH
{
    CCSprite * spriteWithSpriteFrameNameOrFile(const char *nameOrFile);

    CCArray * frameArrayHelper(const char *name, int capacity);

    CCAnimation * animationHelper(const char *name, int capacity, float delay);

    CCAnimate * animateHelper(const char *name, int capacity, float delay);

    CCRepeatForever * foreverAnimateHelper(const char *name, int capacity, float delay);
}
