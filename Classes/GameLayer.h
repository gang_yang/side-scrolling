#pragma once
#include "cocos2d.h"
#include "JoyStick.h"
USING_NS_CC;

class Hero;
class HudLayer;

#ifdef USING_FD // using fastdelegate 
class GameLayer : public CCLayer
#else
class GameLayer : public CCLayer, public SimpleDPadDelegate
#endif
{
    CC_SYNTHESIZE(int, m_level, Level);
    CC_SYNTHESIZE(HudLayer*, m_hud, Hud);
    CC_SYNTHESIZE_RETAIN(CCArray*, m_robots, Robots);

public:
    GameLayer(void);
    ~GameLayer(void);

    static GameLayer* create(int level);

    virtual bool initWithLevel(int level);
    
    virtual void ccTouchesBegan(CCSet *pTouches, CCEvent *pEvent);

    virtual void update(float dt);

    virtual void draw();

    // SimpleDPadDelegate
    void simpleDPadDidChangeDirectionTo(SimpleDPad *dpad, CCPoint direction);
    void simpleDPadIsHoldingDirection(SimpleDPad *dpad, CCPoint direction);
    void simpleDPadTouchEnded(SimpleDPad *dpad);

protected:
    void loadMusic();

    void initTilemap();
    void initHero();

    void initRobots();

    // update hero and robots ' position
    void updatePositions ();
    
    // keep view point center with @pos
    void setViewPointCenter(CCPoint pos);

    //
    void reorderActors();

    // 
    void updateRobots(float dt);

private:
    void endGame();
    void restartGame(CCObject*);

protected:
    CCTMXTiledMap *m_tilemap;
    CCSpriteBatchNode *m_actors;
    Hero    *m_hero;
};

