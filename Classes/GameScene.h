#pragma once

#include "CCHelper.h"

#include "GameLayer.h"
#include "HudLayer.h"

class GameScene : public CCScene
{
    CC_SYNTHESIZE(GameLayer*, m_gameLayer, GameLayer);
    CC_SYNTHESIZE(HudLayer*, m_hudLayer, HudLayer);
public:
    GameScene();
    virtual bool init();

    CREATE_FUNC(GameScene);


};