#include "Hero.h"
#include "CCHelper.h"
#include "LevelDataMgr.h"

bool Hero::init()
{
    bool bRet = false;
    do 
    {
        CC_BREAK_IF(!initWithSpriteFrameName("hero_idle_00.png"));

        // init hero data
        {
            setcenterToBottom(gDataMrg->valueForKey("centerToBottom")->floatValue());
            setCenterToSides(gDataMrg->valueForKey("centerToSides")->floatValue());
            setHitPoints(gDataMrg->valueForKey("hitPoints")->floatValue());
            setDamage(gDataMrg->valueForKey("damage")->floatValue());
            setWalkSpeed(gDataMrg->valueForKey("walkSpeed")->floatValue());
        }

        
        setIdleAction(CCH::foreverAnimateHelper("hero_idle", 6, 1.0/12.0));
        setAttackAction(CCSequence::create(
                CCH::animateHelper("hero_attack_00", 3, 1.0/24.0)
                , CCCallFunc::create(this, callfunc_selector(Hero::idle))
                , NULL)
            );
        setWalkAction(CCH::foreverAnimateHelper("hero_walk", 8, 1.0/12.0));
        setHurtAction(CCSequence::create(CCH::animateHelper("hero_hurt", 3, 1.0/12.0)
            , CCCallFunc::create(this, callfunc_selector(Hero::idle))
            , NULL));
        setKnockedOutAction(CCSequence::create(
            CCH::animateHelper("hero_knockout", 5, 1.0/12.0)
            , CCBlink::create(2.0, 10.0)
            , NULL));
        // Create bounding boxes
        setHitBox(createBoundingBoxWithOrigin(ccp(-getCenterToSides(), -getcenterToBottom())
                                        , CCSizeMake(getCenterToSides()*2, getcenterToBottom()*2)));
        setAttackBox(createBoundingBoxWithOrigin(ccp(getCenterToSides(), -10)
                                                , CCSizeMake(20, 20)));
        bRet = true;
    } while (0);
    return bRet;
}
